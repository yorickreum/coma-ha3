maxDim = 42; % maximale Dimension
maxPot = 21; % maximale Potenz

durations = zeros(maxDim,maxPot,2); % durations speichert die Dimension, die Potenz + 1, die Laufzeit für die selbstgebaute Funktion, die Laufzeit der Matlab-Funtkion
lc = 1; % loop counter
for dim = 1:maxDim 
    X = rand(dim); % wir erstellen in jedem Durchlauf (für jede neue Dimensionsgröße) eine neue zufällig Matrix 
    for pot = 0:maxPot
        
        tic;
        potenz(X, 3);
        tm = toc; % tm enthält nun die Laufzeit der manuellen Potenzberechnung
        
        tic;
        X^3;
        ta = toc; % ta enthält nun die Laufzeit der Potenzberechnung durch Matlab
        
        durations(dim, pot+1, 1) = tm; % pot + 1 damit die Potenz 0 auch abgelegt werden kann
        durations(dim, pot+1, 2) = ta;
        
        lc = lc + 1;
    end
end

% Plot in dependence of dimension
pot = 12; % Potenz fixieren, es soll nur die Dimension variieren
x = 1:1:maxDim; % Dimension variieren bis zu maxDim
figure % neuen Graphen
yman = durations(x, pot+1, 1);
yauto = durations(x, pot+1, 2);
plot(x, yman, x, yauto)
title('Laufzeit in Abhängigkeit von der Zahl der Dimensionen'); % in jeder Dimension wird eine Matrix random erzeugt, maybe stupid?
ylabel('Laufzeit in s')
legend('eigene Potenz', 'Matlab-Potenz');

% Plot in dependence of power
dim = 42; % Dimension fixieren, es soll nur die Potenz variieren
x = 0:1:maxPot; % Potenz variieren bis zu maxPot
figure % neuen, nicht in den anderen rein
yman = durations(dim, x+1, 1);
yauto = durations(dim, x+1, 2);
plot(x, yman, x, yauto)
title('Laufzeit in Abhängigkeit von der Potenz');
ylabel('Laufzeit in s')
legend('eigene Potenz', 'Matlab Potenz');
