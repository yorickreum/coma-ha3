function [A,U] = update(A,U)
% Funktion update updatet eine Matrix A indem sie Ansteckung simuliert.
% Falls ein Element der Matrix zwischen 1 und 3 liegt, wird fuer die
% herumliegenden Elemente die Unterfunktion anstecken ausgefuehrt.
% Dabei ist A die Matrix, welche die Krankheit simuliert und U eine Matrix,
% die speichert, ob in dieser Runde schon einmal angesteckt wurde. Jedem
% Aufruf von update sollte eine 50x50 Nullmatrix als Matrix U uebergeben
% werden

% For-Schleife, die erst die Krankheitszustaende, aus denen angesteckt
% wird, dann Zeilen und Spalten der Matrix durchlaeuft
for z = 1:3
    for j = 1:50
        for k = 1:50
            if A(j,k) == z
                if ~(j==1 || j==50 || k==1 || k==50)
                    for x = -1:1
                        [i,U] = anstecken(z,j+x,k-1,U);                     % Ruft Unterfunktion anstecken auf und speichert neue Matrix U und den Wert i
                        A(j+x,k+1) = A(j+x,k+1) + i;                        % inkrementiert ein Element von A falls i = 1
                        [i,U] = anstecken(z,j+x,k-1,U);
                        A(j+x,k-1) = A(j+x,k-1) + i;
                    end
                    [i,U] = anstecken(z,j+1,k,U);
                    A(j+1,k) = A(j+1,k) + i;
                    [i,U] = anstecken(z,j-1,k,U);
                    A(j-1,k) = A(j-1,k) + i;
                end
            end
        end
    end
end
end

% Unterfunktion
function [i,U] = anstecken(z,j,k,U)
% Funktion anstecken, die regelt, ob ein Element inkrementiert wird. Dazu
% bekommt die Funktion den Krankheitszustand des ansteckenden Elements (z)
% uebergeben, die Koordinaten des anzusteckenden Elements (j, k) und die
% Matrix U, in der gespeichert wird, ob im Ablauf von Update schon eine
% Ansteckung vorgenommen wurde. Die Funktion gibt i=1 falls angesteckt und
% i=0 falls nicht angesteckt zurueck. Ebenfalls gibt sie die Matrix U
% zurueck.
    switch z
        case 1
            if rand() <= 0.125
                U(j,k) = U(j,k) + 1;                                        % Position als angesteckt definieren
                i = 1;                                                      % anstecken
            else
                i = 0;                                                      % nicht anstecken
            end
        case 2
            if rand() <= 0.25
                U(j,k) = U(j,k) + 1;
                i = 1;
            else
                i = 0;
            end
        case 3
            if rand() <= 0.375
                U(j,k) = U(j,k) + 1; 
                i = 1;
            else
                i = 0;
            end
        otherwise
            i = 0;
    end
    if U(j,k) >= 2
        i = 0;                                                              % Falls schon angesteckt
    end
end