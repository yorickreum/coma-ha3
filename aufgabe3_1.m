n = input('Geben Sie die Anzahl der Zeitschritte an: ');% Benutzereingabe der Schritte

A = zeros(50);                                          % Speicherplatz fuer A reservieren

% Zufallszahlen erzeugen
zufall = rand(1,25);                                    % Zufallszahlen erzeugen
for i = 1:25
    zufall(i) = floor(mod(zufall(i)*1000000,2500))+1;   % Zufallszahlen normieren
    if i > 1 && zufall(i) == zufall(i-1)
        zufall(i) = zufall(i-1)+1;                      % Duplikate ausschliessen, so dass am Ende 25 verschiedene Werte im Array stehen
    end
end
zufall = sort(zufall);                                  % Zufallszahlen sortieren

% Matrix A erstellen
x = 1;
taken = 1;
for i = 1:50
    for j = 1:50
        if taken < 26 && zufall(taken) == x             % Wenn x == Zufallswert
            A(i,j) = 1;                                 % Eintrag hat leichte Symptome
            taken = taken + 1;
        end
        x = x+1;
    end
end

% Zeichenschritte auszeichnen
zs = zeros(1,10);                                       % Speicherplatz fuer Schritte reservieren
for i = 1:10
    zs(i) = floor(i*n/10);                              % n in 10 Schritte aufteilen, an denen gespeichert werden muss
end

% Simulation
ink = 1;
for i = 1:n
    U = zeros(50);                                      % Matrix U zuruecksetzen
    [A,U] = update(A,U);                                % U updaten
    if n<10
        draw(A);                                        % Falls n<10 ergibt es keinen Sinn 10 Mal zu zeichnen
    elseif i == zs(ink)
        draw(A);                                        % Matrix zeichen
        ink = ink+1;                                    % inkrementieren, da dieser Schritt gezeichnet wurde
    end
end