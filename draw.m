function draw(A)
% Funktion draw, welche die Matrix graphisch darstellt

D = zeros(50,50,3);                                 % Matrix, in der die Farben dargestellt werden

% Alle Elemente der Matrix A werden durchgelaufen und so die Matrix D
% gefuellt
for j = 1:50
    for k = 1:50
        switch A(j,k)
            case 0
                D(j,k,:)=[1 1 1];                   % kein Kontakt = weiss
            case 1
                D(j,k,:)=[1 1 0];                   % leichte Symptome = gelb
            case 2
                D(j,k,:)=[0.9100 0.4100 0.1700];    % starke Symptome = orange
            case 3
                D(j,k,:)=[1 0 0];                   % krank = rot
            otherwise
                D(j,k,:)=[0 1 0];                   % immun = gruen
        end
    end
end
imshow(D);                                          % Zeigt Bild an
end